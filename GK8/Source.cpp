/*
(c) Janusz Ganczarski
http://www.januszg.hg.pl
JanuszG@enter.net.pl
*/

#include <GL/glut.h>
#include <stdlib.h>


// stae do obsugi menu podrcznego

enum
{
	CSG_A, // tylko obiekt A
	CSG_B, // tylko obiekt A
	CSG_A_OR_B, // A OR B
	CSG_A_AND_B, // A AND B
	CSG_A_SUB_B, // A SUB B
	CSG_B_SUB_A, // B SUB A
	FULL_WINDOW, // aspekt obrazu - cae okno
	ASPECT_1_1, // aspekt obrazu 1:1
	EXIT // wyjcie
};

// aspekt obrazu

int aspect = FULL_WINDOW;

// rozmiary bryy obcinania

const GLdouble left = -2.0;
const GLdouble right = 2.0;
const GLdouble bottom = -2.0;
const GLdouble top = 2.0;
const GLdouble near1 = 3.0;
const GLdouble far1 = 7.0;

// kty obrotu sceny

GLfloat rotatex = 0.0;
GLfloat rotatey = 0.0;

// wskanik nacinicia lewego przycisku myszki

int button_state = GLUT_UP;

// pooenie kursora myszki

int button_x, button_y;

// identyfikatory list wywietlania

GLint A, B;
GLint C;

// rodzaj operacji CSG

int csg_op = CSG_A_OR_B;

// ustawienie bufora szablonowego tak, aby wydzieli i wywietli
// te elementy obiektu A, ktre znajduj si we wntrzu obiektu B;
// stron (przedni lub tyln) wyszukiwanych elementw obiektu A
// okrela parametr cull_face

void Inside(GLint A, GLint B, GLenum cull_face, GLenum stencil_func)
{
	// pocztkowo rysujemy obiekt A w buforze gbokoci przy
	// wyczonym zapisie skadowych RGBA do bufora kolorw

	// wczenie testu bufora gbokoci
	glEnable(GL_DEPTH_TEST);

	// wyczenie zapisu skadowych RGBA do bufora kolorw
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	// rysowanie wybranej strony wieloktw
	glCullFace(cull_face);

	// wywietlenie obiektu A
	glCallList(A);

	// nastpnie przy uyciu bufora szablonowego wykrywamy te elementy
	// obiektu A, ktre znajduj si wewntrz obiektu B; w tym celu
	// zawarto bufora szablonowego jest zwikszana o 1, wszdzie gdzie
	// bd przednie strony wieloktw skadajcych si na obiekt B

	// wyczenie zapisu do bufora gbokoci
	glDepthMask(GL_FALSE);

	// wczenie bufora szablonowego
	glEnable(GL_STENCIL_TEST);

	// test bufora szablonowego
	glStencilFunc(GL_ALWAYS, 0, 0);

	// okrelenie operacji na buforze szablonowym
	glStencilOp(GL_KEEP, GL_KEEP, GL_INCR);

	// rysowanie tylko przedniej strony wieloktw
	glCullFace(GL_BACK);

	// wywietlenie obiektu B
	glCallList(B);

	// w kolejnym etapie zmniejszamy zawarto bufora szablonowego o 1
	// wszdzie tam, gdzie s tylne strony wieloktw obiektu B

	// okrelenie operacji na buforze szablonowym
	glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);

	// rysowanie tylko tylnej strony wieloktw
	glCullFace(GL_FRONT);

	// wywietlenie obiektu B
	glCallList(B);

	// dalej wywietlamy te elementy obiektu A, ktre
	// znajduj si we wntrzu obiektu B

	// wczenie zapisu do bufora gbokoci
	glDepthMask(GL_TRUE);

	// wczenie zapisu skadowych RGBA do bufora kolorw
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	// test bufora szablonowego
	glStencilFunc(stencil_func, 0, 1);

	// wyczenie testu bufora gbokoci
	glDisable(GL_DEPTH_TEST);

	// rysowanie wybranej strony wieloktw
	glCullFace(cull_face);

	// wywietlenie obiektu A
	glCallList(A);

	// wyczenie bufora szablonowego
	glDisable(GL_STENCIL_TEST);
}

// funkcja generujca scen 3D

void inline AND() {
	Inside(C, B, GL_BACK, GL_NOTEQUAL);

	// wyczenie zapisu skadowych RGBA do bufora kolorw
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	// wczenie testu bufora gbokoci
	glEnable(GL_DEPTH_TEST);

	// wyczenie bufora szablonowego
	glDisable(GL_STENCIL_TEST);

	// wybr funkcji do testu bufora gbokoci
	glDepthFunc(GL_ALWAYS);

	// wywietlenie obiektu B
	glCallList(B);

	// wybr funkcji do testu bufora gbokoci
	glDepthFunc(GL_LESS);

	// elementy obiektu B znajdujce si we wntrzu A
	Inside(B, C, GL_BACK, GL_NOTEQUAL);
}
void inline SUB() {
	// elementy obiektu A znajdujce si we wntrzu B
	Inside(A, B, GL_FRONT, GL_NOTEQUAL);

	// wyczenie zapisu skadowych RGBA do bufora kolorw
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	// wczenie testu bufora gbokoci
	glEnable(GL_DEPTH_TEST);

	// wyczenie bufora szablonowego
	glDisable(GL_STENCIL_TEST);

	// wybr funkcji do testu bufora gbokoci
	glDepthFunc(GL_ALWAYS);

	// wywietlenie obiektu B
	glCallList(B);

	// wybr funkcji do testu bufora gbokoci
	glDepthFunc(GL_LESS);

	// elementy obiektu B znajdujce si we wntrzu A
	Inside(B, A, GL_BACK, GL_EQUAL);
}

void DisplayScene()
{
	// kolor ta - zawarto bufora koloru
	glClearColor(1.0, 1.0, 1.0, 1.0);

	// czyszczenie bufora koloru, bufora gbokoci i bufora szablonowego
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// wybr macierzy modelowania
	glMatrixMode(GL_MODELVIEW);

	// macierz modelowania = macierz jednostkowa
	glLoadIdentity();

	// przesunicie ukadu wsprzdnych obiektw do rodka bryy odcinania
	glTranslatef(0, 0, -(near1 + far1) / 2);

	// obroty caej sceny
	glRotatef(rotatex, 1.0, 0.0, 0.0);
	glRotatef(rotatey, 0.0, 1.0, 0.0);

	// wczenie owietlenia
	glEnable(GL_LIGHTING);

	// wczenie wiata GL_LIGHT0
	glEnable(GL_LIGHT0);

	// wczenie automatycznej normalizacji wektorw normalnych
	glEnable(GL_NORMALIZE);

	// wczenie obsugi waciwoci materiaw
	glEnable(GL_COLOR_MATERIAL);

	// waciwoci materiau okrelone przez kolor wierzchokw
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// wczenie rysowania wybranej strony wieloktw
	glEnable(GL_CULL_FACE);

	// operacja CSG - tylko obiekt A
	SUB();
	AND();
	// skierowanie polece do wykonania
	glFlush();

	// zamiana buforw koloru
	glutSwapBuffers();
}

// zmiana wielkoci okna

void Reshape(int width, int height)
{
	// obszar renderingu - cae okno
	glViewport(0, 0, width, height);

	// wybr macierzy rzutowania
	glMatrixMode(GL_PROJECTION);

	// macierz rzutowania = macierz jednostkowa
	glLoadIdentity();

	// parametry bryy obcinania
	if (aspect == ASPECT_1_1)
	{
		// wysoko okna wiksza od wysokoci okna
		if (width < height && width > 0)
			glFrustum(left, right, bottom * height / width, top * height / width, near1, far1);
		else

			// szeroko okna wiksza lub rwna wysokoci okna
			if (width >= height && height > 0)
				glFrustum(left * width / height, right * width / height, bottom, top, near1, far1);

	}
	else
		glFrustum(left, right, bottom, top, near1, far1);

	// generowanie sceny 3D
	DisplayScene();
}

// obsuga przyciskw myszki

void MouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		// zapamitanie stanu lewego przycisku myszki
		button_state = state;

		// zapamitanie pooenia kursora myszki
		if (state == GLUT_DOWN)
		{
			button_x = x;
			button_y = y;
		}
	}
}

// obsuga ruchu kursora myszki

void MouseMotion(int x, int y)
{
	if (button_state == GLUT_DOWN)
	{
		rotatey += 30 * (right - left) / glutGet(GLUT_WINDOW_WIDTH) *(x - button_x);
		button_x = x;
		rotatex -= 30 * (top - bottom) / glutGet(GLUT_WINDOW_HEIGHT) *(button_y - y);
		button_y = y;
		glutPostRedisplay();
	}
}

// obsuga menu podrcznego

void Menu(int value)
{
	switch (value)
	{
		// operacja CSG
	case CSG_A:
	case CSG_B:
	case CSG_A_OR_B:
	case CSG_A_AND_B:
	case CSG_A_SUB_B:
	case CSG_B_SUB_A:
		csg_op = value;
		DisplayScene();
		break;

		// obszar renderingu - cae okno
	case FULL_WINDOW:
		aspect = FULL_WINDOW;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;

		// obszar renderingu - aspekt 1:1
	case ASPECT_1_1:
		aspect = ASPECT_1_1;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;

		// wyjcie
	case EXIT:
		exit(0);
	}
}

// utworzenie list wywietlania

void GenerateDisplayLists()
{
	// generowanie identyfikatora pierwszej listy wywietlania
	A = glGenLists(1);

	// pierwsza lista wywietlania
	glNewList(A, GL_COMPILE);

	// czerwony szecian
	glColor4f(1.f, 0.f, 0.f, 1.f);
	glutSolidCone(5, 3, 10, 10);

	// koniec pierwszej listy wywietlania
	glEndList();

	// generowanie identyfikatora drugiej listy wywietlania
	B = glGenLists(1);

	// druga lista wywietlania
	glNewList(B, GL_COMPILE);

	// zielona kula
	glColor4f(0.f, 1.0f, 0.f, 1.f);
	glutSolidSphere(1.5, 30, 30);

	// koniec drugiej listy wywietlania
	glEndList();

	C = glGenLists(1);

	// druga listywietlania
	glNewList(C, GL_COMPILE);

	// zielona kula
	glColor4f(0.f, 0.0f, 1.f, 1.f);

	glutSolidIcosahedron();

	// koniec drugiej listy wywietlania
	glEndList();
}

int main(int argc, char * argv[])
{
	// inicjalizacja biblioteki GLUT
	glutInit(&argc, argv);

	// inicjalizacja bufora ramki
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);

	// rozmiary gwnego okna programu
	glutInitWindowSize(500, 500);

	// utworzenie gwnego okna programu
	glutCreateWindow("CSG");

	// doczenie funkcji generujcej scen 3D
	glutDisplayFunc(DisplayScene);

	// doczenie funkcji wywoywanej przy zmianie rozmiaru okna
	glutReshapeFunc(Reshape);

	// obsuga przyciskw myszki
	glutMouseFunc(MouseButton);

	// obsuga ruchu kursora myszki
	glutMotionFunc(MouseMotion);

	// utworzenie podmenu - Operacja CSG
	int MenuCSGOp = glutCreateMenu(Menu);
	glutAddMenuEntry("A", CSG_A);
	glutAddMenuEntry("B", CSG_B);
	glutAddMenuEntry("A OR B", CSG_A_OR_B);
	glutAddMenuEntry("A AND B", CSG_A_AND_B);
	glutAddMenuEntry("A SUB B", CSG_A_SUB_B);
	glutAddMenuEntry("B SUB A", CSG_B_SUB_A);

	// utworzenie podmenu - Aspekt obrazu
	int MenuAspect = glutCreateMenu(Menu);
#ifdef WIN32

	glutAddMenuEntry("Aspekt obrazu - cae okno", FULL_WINDOW);
#else

	glutAddMenuEntry("Aspekt obrazu - cale okno", FULL_WINDOW);
#endif

	glutAddMenuEntry("Aspekt obrazu 1:1", ASPECT_1_1);

	// menu gwne
	glutCreateMenu(Menu);
	glutAddSubMenu("Operacja CSG", MenuCSGOp);

#ifdef WIN32

	glutAddSubMenu("Aspekt obrazu", MenuAspect);
	glutAddMenuEntry("Wyjcie", EXIT);
#else

	glutAddSubMenu("Aspekt obrazu", MenuAspect);
	glutAddMenuEntry("Wyjscie", EXIT);
#endif

	// okrelenie przycisku myszki obsugujcego menu podrczne
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	// utworzenie list wywietlania
	GenerateDisplayLists();

	// wprowadzenie programu do obsugi ptli komunikatw
	glutMainLoop();
	return 0;
}